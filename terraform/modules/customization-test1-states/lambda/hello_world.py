import inspect
from typing import TYPE_CHECKING, Any, Dict, Union

def lambda_handler(event: Dict[str, Any], context: Union[Dict[str, Any], None]) -> None:

    target_account = event["account_info"]["account"]["id"]
    print(target_account)

    message = "Hello, the account id is " + target_account
    return {
       'message' : message
    }
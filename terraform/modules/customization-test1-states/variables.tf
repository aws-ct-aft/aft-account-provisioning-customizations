variable "customization_name" {
    type = string
}

variable "customization_state_machine_definition" {
    type = string
}

variable "function_name" {
    type = string
}

variable "function_exception_name" {
    type = string
}
# define lambda function
data "archive_file" "lambda_code" {
    type        = "zip"
    source_dir  = "${path.module}/lambda"
    output_path = "${path.module}/archive/${var.customization_name}_lambda.zip"
}

resource "aws_lambda_function" "lambda_func" {
    filename            = "${path.module}/archive/${var.customization_name}_lambda.zip"
    source_code_hash    = data.archive_file.lambda_code.output_base64sha256
    function_name       = var.function_name
    role                = aws_iam_role.aft_customization_states.arn
    handler             = "${var.function_name}.lambda_handler"
    runtime             = "python3.8"
    timeout             = 600
}

resource "aws_lambda_function" "lambda_exception_func" {
    filename            = "${path.module}/archive/${var.customization_name}_lambda.zip"
    source_code_hash    = data.archive_file.lambda_code.output_base64sha256
    function_name       = var.function_exception_name
    role                = aws_iam_role.aft_customization_states.arn
    handler             = "${var.function_exception_name}.lambda_handler"
    runtime             = "python3.8"
}
resource "aws_sfn_state_machine" "customization_state_machine" {
  name     = var.customization_name
  role_arn = aws_iam_role.aft_customization_states.arn
  definition = templatefile("${path.module}/states/${var.customization_state_machine_definition}", { 
      step_function_arn            = "${aws_lambda_function.lambda_func.arn}",
      step_function_exception_arn  = "${aws_lambda_function.lambda_exception_func.arn}"
  })
}


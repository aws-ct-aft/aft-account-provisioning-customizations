{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*",
                "sns:*",
                "states:*",
                "sqs:*",
                "lambda:InvokeFunction",
                "iam:ListRoles",
                "iam:ListInstanceProfiles",
                "iam:PassRole",
                "cloudwatch:*"
            ],
            "Resource": [
                "${account_provisioning_customizations_sfn_arn}",
                "${account_provisioning_lambda_func_arn}",
                "${account_provisioning_lambda_func_exception_arn}"
            ]
        }
    ]
}

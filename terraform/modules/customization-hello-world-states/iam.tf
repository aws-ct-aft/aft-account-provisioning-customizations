resource "aws_iam_role" "aft_customization_states" {
  name               = "${var.customization_name}_role"
  assume_role_policy = templatefile("${path.module}/iam/trust-policies/states.tpl", { none = "none" })
}

resource "aws_iam_role_policy" "aft_customization_states" {
  name = "${var.customization_name}_role_policy"
  role = aws_iam_role.aft_customization_states.id

  policy = templatefile("${path.module}/iam/role-policies/iam-aft-states.tpl", {
    account_provisioning_customizations_sfn_arn = aws_sfn_state_machine.customization_state_machine.arn
  })
}

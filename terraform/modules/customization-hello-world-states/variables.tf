variable "customization_name" {
    type = string
    default = "customization_state_machine"
}

variable "customization_state_machine_definition" {
    type = string
    default = "hellowold.asl.json"
}
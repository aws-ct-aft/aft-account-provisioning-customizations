data "aws_iam_policy" "admin_policy" {
  name = "AdministratorAccess"
}

resource "aws_iam_role" "aft_states" {
  name               = "aft-account-provisioning-customizations-role"
  assume_role_policy = templatefile("${path.module}/iam/trust-policies/states.tpl", { none = "none" })
}

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
    role        = aws_iam_role.aft_states.id
    policy_arn  = data.aws_iam_policy.admin_policy.arn
}
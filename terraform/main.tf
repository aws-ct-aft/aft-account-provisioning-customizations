# Account Provisioning Customization default state machine, do not change this
module "account_provisioning_default_state_machine" {
    source = "./modules/account-provisioning-customization-states"

    customization_state_arn = module.account_provisioning_customization_state_machine.customization_state_arn
}

# customization state machine, change this to reference the customized state machine module

/*
module "account_provisioning_customization_state_machine" {
    source = "./modules/customization-hello-world-states"
}
*/

module "account_provisioning_customization_state_machine" {
    source = "./modules/customization-test1-states"
    
    customization_name = "customization_test1_state_machine"
    customization_state_machine_definition =  "customization-test1.asl.json"
    function_name = "hello_world"
    function_exception_name = "hello_world_exception"
}
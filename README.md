# aft-account-provisioning-customizations

This is sample repository for `aft-account-provisioning-customizations`. Copy content here into your AFT `aft-account-provisioning-customizations` repository

## How to build your own state machine with aft-account-provisioning-customizations

- [ ] Create your own state machine module, package it as a directory 
- [ ] Copy your entrie directory into `modules` directory that under `terraform` directory
- [ ] Reference the module in main.tf file under `terraform` directory, for example:

```
module "account_provisioning_customization_state_machine" {
    source = "./modules/customization-hello-world-states"
}
```

## Reuqirement for customization state machine

The output is required, name of output should be "customization_state_arn". This is because aft-account-provisioning-customizations default state machine moudule required input variable, the variable should be the customization state machine Arn. With this output name, the aft-account-provisioning-customizations default state machine will be able to execute the user defined state machine. This can be done via creating outputs.tf in your own state machine package, here is an example:

```
output "customization_state_arn" {
    value = aws_sfn_state_machine.customization_state_machine.arn
}
```

## Contributor
Jun Chen (jcchenn@amazon.com)
